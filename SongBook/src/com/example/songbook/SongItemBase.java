package com.example.songbook;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import songbook.vaadin.model.SongItem;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;

public class SongItemBase {

	private static Mongo mongoClient;
	private static DBCollection collection;
	static {
		try {
			mongoClient = new Mongo("localhost");
			DB db = mongoClient.getDB("local");
			collection = db.getCollection("test");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public List<SongItem> getInitialState() {
		ArrayList<SongItem> toReturn = new ArrayList<SongItem>();
		DBCursor cursor = collection.find();
		try {
			while (cursor.hasNext()) {
				toReturn.add(DBConverter.convertToItem(cursor.next()));
			}
		} finally {
			cursor.close();
		}
//		DBConverter.convertToItem(collection.findOne());
		
		return toReturn;
	}
}
