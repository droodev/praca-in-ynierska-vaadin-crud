package songbook.vaadin.components.inner;

import songbook.vaadin.model.SongPart;
import songbook.vaadin.model.SongQualifiedPart;

public interface SongWriterController {
	public void fieldAdded(SongPart part);
	public void fieldRemoved(SongQualifiedPart qaulifiedPart);
}
