package songbook.vaadin.components;

import java.util.List;

import songbook.vaadin.model.SongItem;
import songbook.vaadin.model.SongPart;
import songbook.vaadin.model.SongQualifiedPart;

import com.example.songbook.SongItemBase;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class SongTable extends CustomComponent {
	private Table table;
	private HorizontalLayout mainLayout;
	private VerticalLayout rightLayout;
	
	
	private final IndexedContainer container = new IndexedContainer();
	private final SongWriter songWriter;
	
	private Button addButton;
	private Button removeButton;
	
	private void initComponents(){
		table = new Table();
		mainLayout = new HorizontalLayout();
		rightLayout = new VerticalLayout();
		
		addButton = new Button("Dodaj");
		removeButton = new Button("Usu�");
	}
	
	private void composeLayouts(){
		mainLayout.addComponent(table);
		mainLayout.addComponent(rightLayout);
		
		rightLayout.addComponent(addButton);
		rightLayout.addComponent(removeButton);
		
		setCompositionRoot(mainLayout);
	}
	
	public SongTable(final SongWriter songWriter) {
		this.songWriter=songWriter;
		songWriter.setEnabled(false);
		initComponents();
		setUpMainLayout();
		setUpTable();
		
		setUpAddButton(songWriter);
		
		setUpDeleteButton();
		
		
		composeLayouts();
		
		
		
		getDataFromBase();
	}

	private void setUpDeleteButton() {
		removeButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				Object id = table.getValue();
				if(id==null){
					return;
				}
				if(!id.equals(table.getNullSelectionItemId())){
					deleteItemAt(id);
				}
				table.select(table.getNullSelectionItemId());
				songWriter.setEnabled(false);
				table.markAsDirty();
				
			}
		});
	}

	private void setUpAddButton(final SongWriter songWriter) {
		addButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				songWriter.setEnabled(true);
				SongItem songItem = new SongItem();
				addSongItem(songItem);
				songWriter.reload(songItem);
				table.select(table.lastItemId());
				table.markAsDirty();
			}
		});
	}

	private void getDataFromBase() {
		List<SongItem> items = new SongItemBase().getInitialState();
		for(SongItem newItem: items){
			addSongItem(newItem);
		}
		markAsDirty();
	}
	
	private void addSongItem(SongItem songItem){
		songItem.bindWithIndexedItem(getNewItem());
	}

	private void setUpTable() {	
		container.addContainerProperty(new SongQualifiedPart(0,SongPart.TITLE), String.class, "bez tytu�u");
		table.setContainerDataSource(container);
		table.setCaption("Piosenki");
		table.setSelectable(true);
	}

	private void setUpMainLayout() {
		mainLayout.setSpacing(true);
	}
	
	
	public Item getNewItem(){
		return container.getItem(container.addItem());
	}
	
	private void deleteItemAt(Object id){
		container.removeItem(id);
	}
	
	public void refresh(){
		table.refreshRowCache();
		table.markAsDirty();
	}
}
