package songbook.vaadin.components;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import songbook.vaadin.components.inner.SongWriterController;
import songbook.vaadin.model.SongItem;
import songbook.vaadin.model.SongPart;
import songbook.vaadin.model.SongQualifiedPart;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class SongWriter extends CustomComponent implements SongWriterController {

	private  Map<SongQualifiedPart, RemovableTextArea> textAreasMap;
	private  SongItem boundItem;
	private  FieldGroup binder;
	
	//private SongTable tab;

	private HorizontalLayout mainLayout;
	private VerticalLayout addingLayout;
	private FormLayout formLayout;
	private TextField titleField;
	private Button commitButton;

	private EnumMap<SongPart, Button> addingButtons = new EnumMap<SongPart, Button>(
			SongPart.class);

	private void initComponents() {

		mainLayout = new HorizontalLayout();
		addingLayout = new VerticalLayout();
		formLayout = new FormLayout();
		titleField = new TextField("Tytu�");
		commitButton = new Button("Zapisz");

		addingButtons.put(SongPart.CHORUS, new AddingButton(SongPart.CHORUS,
				this, "Dodaj refren"));
		addingButtons.put(SongPart.BRIDGE, new AddingButton(SongPart.BRIDGE,
				this, "Dodaj bridge"));
		addingButtons.put(SongPart.VERSE, new AddingButton(SongPart.VERSE,
				this, "Dodaj zwrotk�"));
	}

	private void setUpTitleField() {
		binder.bind(titleField, new SongQualifiedPart(0, SongPart.TITLE));
	}

	private void composeLayouts() {
		mainLayout.removeAllComponents();
		mainLayout.addComponent(formLayout);
		mainLayout.addComponent(addingLayout);

		for (Button button : addingButtons.values()) {
			addingLayout.addComponent(button);
		}
		addingLayout.addComponent(commitButton);

		formLayout.addComponent(titleField);

		setCompositionRoot(mainLayout);
	}

	private void setUpMainLayout() {
		mainLayout.setSpacing(true);
	}

	public SongWriter(SongItem boundItem/*, SongTable tab*/) {
		//this.tab = tab;
		textAreasMap = new HashMap<SongQualifiedPart, RemovableTextArea>();
		this.boundItem = boundItem;
		binder = new FieldGroup(boundItem);

		initComponents();

		setUpTitleField();
		setUpMainLayout();
		setUpCommitButton();
		composeLayouts();
	}

	public void reload(SongItem boundItem){
		textAreasMap = new HashMap<SongQualifiedPart, RemovableTextArea>();
		this.boundItem = boundItem;
		binder = new FieldGroup(boundItem);
		
		setUpTitleField();
		setUpMainLayout();
		setUpCommitButton();
		composeLayouts();
		
		
		markAsDirty();
	}
	
	private void setUpCommitButton() {
		commitButton.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				try {
					binder.commit();
					boundItem.changeCommited();
					//tab.refresh();
				} catch (CommitException e) {
					e.printStackTrace(System.err);
				}
			}
		});
	}

	@Override
	public void fieldAdded(SongPart part) {
		SongQualifiedPart newPartId = boundItem.getNextPart(part);
		boundItem.addItemProperty(newPartId, new ObjectProperty<String>(
				"Starting"));
		RemovableTextArea newTextArea = new RemovableTextArea(this, newPartId);
		binder.bind(newTextArea, newPartId);
		textAreasMap.put(newPartId, newTextArea);

		formLayout.addComponent(newTextArea);
		formLayout.markAsDirty();
	}

	@Override
	public void fieldRemoved(SongQualifiedPart qualifiedPart) {

		binder.unbind(textAreasMap.get(qualifiedPart));
		formLayout.removeComponent(textAreasMap.get(qualifiedPart));

		LinkedList<SongQualifiedPart> greaterList = extractGreaterParts(qualifiedPart);

		for (SongQualifiedPart greaterPart : greaterList) {
			RemovableTextArea partTextArea = textAreasMap.get(greaterPart);
			textAreasMap.remove(greaterPart);

			binder.unbind(partTextArea);

			SongQualifiedPart predecessorPart = greaterPart.getPredecessor();

			partTextArea.setQualifiedName(predecessorPart);
			textAreasMap.put(predecessorPart, partTextArea);
			binder.bind(partTextArea, predecessorPart);
		}

		boundItem.removeItemProperty(qualifiedPart);
		formLayout.markAsDirty();
	}

	private LinkedList<SongQualifiedPart> extractGreaterParts(
			SongQualifiedPart qualifiedPart) {
		@SuppressWarnings("unchecked")
		Collection<SongQualifiedPart> allItems = (Collection<SongQualifiedPart>) boundItem
				.getItemPropertyIds();
		LinkedList<SongQualifiedPart> concernedItems = new LinkedList<SongQualifiedPart>();
		for (SongQualifiedPart part : allItems) {
			if (part.getPart() == qualifiedPart.getPart()
					&& part.getOrdinalNumber() > qualifiedPart
							.getOrdinalNumber()) {
				concernedItems.add(part);
			}
		}
		return concernedItems;
	}

}
