package songbook.vaadin.model;

public enum SongPart {
	CHORUS("Refren"), BRIDGE("Bridge"), VERSE("Zwrotka"), TITLE("Tytu�");
	
	private final String fullName;

	private SongPart(String fullName) {
		this.fullName = fullName;
	}
	
	
	@Override
	public String toString() {
		return fullName;
	}
}
